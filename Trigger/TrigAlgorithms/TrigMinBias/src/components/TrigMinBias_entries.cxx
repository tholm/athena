#include "../TrigTrackCounter.h"
#include "../TrigTrackCounterHypo.h"
#include "../TrigVertexCounter.h"
#include "../TrigVertexCounterHypo.h"

DECLARE_COMPONENT( TrigTrackCounter )
DECLARE_COMPONENT( TrigTrackCounterHypo )
DECLARE_COMPONENT( TrigVertexCounter )
DECLARE_COMPONENT( TrigVertexCounterHypo )

